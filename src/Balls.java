
public class Balls {

   enum Color {green, red};
   
   public static void main (String[] param) {
      // for debugging
   }
   
    /**
     * This method rearranges the array, so that all
     * red balls are at the beginning and all green
     * balls are at the end of the array.
     *
     * @param balls an array that needs to be sorted
     */
   public static void reorder (Color[] balls) {
       if (balls.length > 2) {
           int greenEnd = balls.length - 1;
           boolean stop = false;
           for (int i = 0; i < balls.length; i++) {
               if (stop == true){break;}
               if (balls[i] == Color.red) {continue;}
               else {
                   for (int j = greenEnd; j > i; j--) {
                       if (balls[j] == Color.red) {
                           balls[i] = balls[j];
                           balls[j] = Color.green;
                           greenEnd = j - 1;
                           break;
                       }
                       if (j == i + 1) {
                           stop = true;
                       }
                   }
               }
           }
       }
   }
}

